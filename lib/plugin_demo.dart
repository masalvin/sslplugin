import 'dart:async';

import 'package:flutter/services.dart';

class PluginDemo {
  static const MethodChannel _channel =
      const MethodChannel('plugin_demo');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<Null> showToast(String msg) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("msg", () => msg);
    await _channel.invokeMethod('showToast', args);
    return null;
  }

  static Future<String> getResponse(String amount) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("amount", () => amount);
    await _channel.invokeMethod('getResponse', args);
    return null;
  }

}
