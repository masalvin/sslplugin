package com.example.plugin_demo;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.sslwireless.sslcommerzlibrary.model.initializer.SSLCommerzInitialization;
import com.sslwireless.sslcommerzlibrary.model.response.TransactionInfoModel;
import com.sslwireless.sslcommerzlibrary.view.singleton.IntegrateSSLCommerz;
import com.sslwireless.sslcommerzlibrary.viewmodel.listener.TransactionResponseListener;

import io.flutter.Log;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/** PluginDemoPlugin */
public class PluginDemoPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {

  protected Activity activity;
  protected Context mContext;
 


  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {

    final MethodChannel channel = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "plugin_demo");
    channel.setMethodCallHandler(this);
    this.mContext = flutterPluginBinding.getApplicationContext();

  }

  public void registerWith(Registrar registrar) {
    final MethodChannel channel = new MethodChannel(registrar.messenger(), "plugin_demo");
    channel.setMethodCallHandler(this);
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    if (call.method.equals("getPlatformVersion")) {
      result.success("Android " + android.os.Build.VERSION.RELEASE);
    } else if(call.method.equals("showToast")){
      String msg = call.argument("msg").toString();
      Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
    } else if(call.method.equals("getResponse")) {
      String amount = call.argument("amount").toString();

      final SSLCommerzInitialization sslCommerzInitialization = new SSLCommerzInitialization("self5e4a7bc6ae","self5e7bc6ae@ssl",
              5.0,
              "BDT",
              "suriya10",
              "FOOD",
              "SANDBOX");

      Log.d("instanceof", String.valueOf(activity instanceof AppCompatActivity));
      Log.d("activity", String.valueOf(activity));

      IntegrateSSLCommerz
              .getInstance((AppCompatActivity)mContext)
              .addSSLCommerzInitialization(sslCommerzInitialization)
              .buildApiCall(new TransactionResponseListener() {
                @Override
                public void transactionSuccess(TransactionInfoModel transactionInfoModel) {
                  Log.d("transactionSuccess", transactionInfoModel.toString());

                }

                @Override
                public void transactionFail(String s) {
                  Log.d("transactionFail", s);

                }

                @Override
                public void merchantValidationError(String s) {
                  Log.d("merchantValidationError", s);

                }
              });
      result.success("Android Called");

    }

    else {
      result.notImplemented();
    }
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
  }

  @Override
  public void onAttachedToActivity(ActivityPluginBinding binding) {
    Log.d("onAttachedToActivity", "Called");
    this.activity = binding.getActivity();
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() {

  }

  @Override
  public void onReattachedToActivityForConfigChanges(ActivityPluginBinding binding) {

  }

  @Override
  public void onDetachedFromActivity() {

  }

}
